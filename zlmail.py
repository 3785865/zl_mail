#!/user/bin/python
# -*- coding:utf-8 -*-

import smtplib
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.header import Header
from email import encoders
import config

email_subject = config.mymail['subject'] #邮件标题
email_content = config.mymail['content']#正文
email_from = config.mymail['form'] #发件地址,中文和发件地址的组合
email_content_html = config.mymail['content_html'] #正文内容
source_path = r"erweima.png" #附件路径和名
part_name = 'erweima.png'#附件名
email_host = config.mymail['host']  # smtp 邮件服务器
host_port = config.mymail['port']   # smtp 邮件服务器端口：SSL 连接
from_addr = config.mymail['form'][0] # 发件地址，只包含发件地址
pwd =  config.mymail['password']  # 发件地址的授权码，而非密码
#注意 收件人地址 有两个，一个是显示地址， 一个是实际地址，显示地址再 get_email_obj中，实际地址再下面
to_addr_list = config.mymail['to'] #收件地址


# Python对SMTP支持有smtplib和email两个模块，email负责构造邮件，smtplib负责发送邮件。
def get_email_obj(email_subject, email_from, to_addr_list):

    '''
    构造邮件对象，并设置邮件主题、发件人、收件人，最后返回邮件对象
    :param email_subject:邮件主题
    :param email_from:发件人
    :param to_addr_list:收件人列表 仅仅用来显示，实际收件人 由send_email调用
    :return :邮件对象 email_obj
    '''
    # 构造 MIMEMultipart 对象做为根容器
    email_obj = MIMEMultipart()
    email_to = to_addr_list  # 将收件人地址用“,”连接
    print(','.join(email_to))

    # 邮件主题、发件人、收件人
    email_obj['Subject'] = Header(email_subject, 'utf-8')
    #网易收件人不能有To utf-8,不能有From
    email_obj['From'] = Header(email_from, 'utf-8')
    #email_obj['To'] = Header(email_to, 'utf-8')
    email_obj['To'] =  Header(','.join(email_to))
    return email_obj


def attach_content(email_obj, email_content, content_type=config.mymail['content_type'], charset='utf-8'):
    '''
    创建邮件正文，并将其附加到跟容器：邮件正文可以是纯文本，也可以是HTML（为HTML时，需设置content_type值为 'html'）
    :param email_obj:邮件对象
    :param email_content:邮件正文内容
    :param content_type:邮件内容格式 'plain'、'html'..，默认为纯文本格式 'plain'
    :param charset:编码格式，默认为 utf-8
    :return:
    '''
    content = MIMEText(email_content, content_type, charset)  # 创建邮件正文对象
    email_obj.attach(content)  # 将邮件正文附加到根容器

def attach_part(email_obj, source_path, part_name):
    '''
    添加附件：附件可以为照片，也可以是文档
    :param email_obj:邮件对象
    :param source_path:附件源文件路径
    :param part_name:附件名
    :return:
    '''
    part = MIMEBase('application', 'octet-stream')  # 'octet-stream': binary data   创建附件对象
    part.set_payload(open(source_path, 'rb').read())  # 将附件源文件加载到附件对象
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', 'attachment; filename="%s"' % part_name)  # 给附件添加头文件
    email_obj.attach(part)  # 将附件附加到根容器

def send_email(email_obj, email_host=email_host, host_port=host_port, from_addr=from_addr, pwd=pwd, to_addr_list=to_addr_list):
    '''
    发送邮件
    :param email_obj:邮件对象
    :param email_host:SMTP服务器主机
    :param host_port:SMTP服务端口号
    :param from_addr:发件地址
    :param pwd:发件地址的授权码，而非密码
    :param to_addr_list:收件地址
    :return:发送成功，返回 True；发送失败，返回 False
    '''
    try:
        '''
            # import smtplib
            # smtp_obj = smtplib.SMTP([host[, port[, local_hostname]]] )
                # host: SMTP服务器主机。
                # port: SMTP服务端口号，一般情况下SMTP端口号为25。
            # smtp_obj = smtplib.SMTP('smtp.qq.com', 25)
        '''
        smtp_obj = smtplib.SMTP_SSL(email_host, host_port)  # 连接 smtp 邮件服务器
        smtp_obj.login(from_addr, pwd)
        smtp_obj.sendmail(from_addr, to_addr_list,email_obj.as_string())  # 发送邮件：email_obj.as_string()：发送的信息
        smtp_obj.quit()  # 关闭连接
        print("send successful!")
        return True
    except smtplib.SMTPException:
        print("failure notice!")
        return False


def sendEmail(subject=email_subject,to=to_addr_list,form=email_from,content=email_content,host=email_host,port=host_port,pwd = pwd):
    print("发件人 %s"% form)
    print("收件人 %s"% to)
    print("标  题 %s"% subject)
    print("正  文 %s"% content)
    get_email_obj1 = get_email_obj(subject,"%s<%s>" % (form[0],form[1]),to)
    attach_content(get_email_obj1, content)
    # print(get_email_obj1.as_string())
    send_email(email_obj=get_email_obj1, email_host=host, host_port=port, from_addr=form[1], pwd=pwd, to_addr_list=to)
    pass




#判断变量类型的函数
def typeof(variate):
    type=None
    if isinstance(variate,int):
        type = "int"
    elif isinstance(variate,str):
        type = "str"
    elif isinstance(variate,float):
        type = "float"
    elif isinstance(variate,list):
        type = "list"
    elif isinstance(variate,tuple):
        type = "tuple"
    elif isinstance(variate,dict):
        type = "dict"
    elif isinstance(variate,set):
        type = "set"
    return type



if __name__ == "__main__":
    # email_obj = get_email_obj(email_subject, email_from, config.mymail['to'])
    # attach_content(email_obj, email_content)
    # attach_part(email_obj, source_path, part_name)
    # send_email(email_obj, email_host, host_port, from_addr, pwd,  config.mymail['to'])
    #send_email(get_email_obj("zl重要消息-请立即去付款", "曾立的付款消息<ps2zzz@qq.com>", "ps2zzz@163.com"))
    pass